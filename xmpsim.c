#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include <pthread.h>
#include <syscall.h>

#define X_INSTRUCTIONS_NOT_NEEDED
#include "xis.h"
#include "xcpu.h"

#define TICK_ARG 1
#define IMAGE_ARG 2
#define QUANTUM_ARG 3
#define CPU_ARG 4

static int ticks;
static int quantum;
static int cores;

static void* run_core(void* arg);



int main( int argc, char **argv ) {

  FILE *fp;
  struct stat fs;
  xcpu *cs;
  unsigned char *mem;
  unsigned int i;
  pthread_t *tid;

  if( ( argc < 5 ) || ( sscanf( argv[TICK_ARG], "%d", &ticks ) != 1 ) ||
      ( ticks < 0 ) || ( sscanf( argv[QUANTUM_ARG], "%d", &quantum ) != 1 ) ||
      ( quantum < 0 ) || ( sscanf( argv[CPU_ARG], "%d", &cores) != 1 ) ||
      ( cores < 1 ) ) {
    fprintf( stderr, "usage: xsim <ticks> <obj file> <quantum>\n" );
    fprintf( stderr,
            "      <ticks> is number instructions to execute (0 = forever)\n" );
    fprintf( stderr,
            "      <image file> xis object file created by or xasxld\n" );
    fprintf( stderr,
            "      <quantum> is the number of ticks between interrupts %s\n",
            "(0 = no interrupts)" );
    fprintf( stderr,
            "      <cores> is the number of cpus to simulate (minimum 1)\n");
    return 1;
  }

  mem = (unsigned char *)malloc( XIS_MEM_SIZE );

  if( !mem ) {
    fprintf( stderr, "error: memory allocation (%d) failed\n", XIS_MEM_SIZE );
    exit( 1 );
  }

  memset( mem, I_BAD, XIS_MEM_SIZE );

  if( stat( argv[IMAGE_ARG], &fs ) ) {
    fprintf( stderr, "error: could not stat image file %s\n", argv[IMAGE_ARG] );
    return 1;
  } else if( fs.st_size > XIS_MEM_SIZE ) {
    fprintf( stderr, "Not enough memory to run all the programs." );
    return 1;
  }

  fp = fopen( argv[IMAGE_ARG], "rb" );
  if( !fp ) {
    fprintf( stderr, "error: could not open image file %s\n", argv[IMAGE_ARG] );
    return 1;
  } else if( fread( mem, 1, fs.st_size, fp ) != fs.st_size ) {
    fprintf( stderr, "error: could not read file %s\n", argv[IMAGE_ARG] );
    return 1;
  }
  fclose( fp );

  /*allocate memory for threads*/
  tid=malloc(sizeof(pthread_t)*cores);

  /*allocate memory for cpu contexts*/
  cs = (xcpu*) malloc((sizeof(xcpu))*cores);

  /*We allocated and assigned memory, now time to allocate cpu's*/
  /*<cores as we start at 0*/
  for(i=0;i<cores;i++){
    /*initialize each cs[i]*/
    cs[i].cpuid=i;
    cs[i].cpunum=cores;
    cs[i].memory=mem;
  }

  /*now create threads*/
  for(i=0;i<cores;i++){
    //printf("Initializing CPU %d\n", (cs[i].cpuid));
    if(pthread_create(&tid[i], NULL, run_core, &cs[i])){
      fprintf(stderr, "Error creating thread\n");
      return 1;
    }
  }
  /*join threads before we can exit*/
  for(i=0;i<cores;i++){
    if(pthread_join(tid[i], NULL)){
      fprintf(stderr, "Error joining threads\n");
      return 2;
    }
  }
  return 0;
}

/*thread entry function*/
static void* run_core(void* arg){
  xcpu* cs = (xcpu*) arg;
  unsigned int* k=malloc(sizeof(int));
  //printf("TID: %d\n", syscall(SYS_gettid));
  for( (*k) = 0; ( ticks < 1 ) || ( (*k) < ticks ); (*k)++ ) {
    if( (*k) && ( quantum > 0 ) && !( (*k) % quantum ) ) {
      if( !xcpu_exception( cs, X_E_INTR ) ) {
        fprintf( stderr, "Exception error, CPU %d has halted.\n", cs->cpuid );
        return NULL;
      }
    }
    if( !xcpu_execute( cs ) ) {
      fprintf( stderr, "CPU %d has halted.\n", cs->cpuid );
      return NULL;
    }
  }
  fprintf( stderr, "CPU %d ran out of time.\n", cs->cpuid );
  return NULL;
}
